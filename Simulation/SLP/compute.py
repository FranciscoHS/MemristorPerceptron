import math

def compute (perceptron_data, input1, input2):
	w1 = perceptron_data[0] # take weights from perceptron parameters
	w2 = perceptron_data[1]
	wb = perceptron_data[2]
	output = 1/(1 + math.exp(-(w1*input1+w2*input2+wb))) 
	return output
