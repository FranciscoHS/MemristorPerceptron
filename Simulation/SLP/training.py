import xlrd       # allows reading from excel files
import xlsxwriter # allows writing to excel files
import math
import matplotlib.pyplot as plt # makes pretty plots
import numpy as np
import random

def train (size, file_name, epochs): # inputs are size of training data set and name of file where it is stored
	training_data = []
	file = xlrd.open_workbook(file_name) # opens excel file 
	sheet = file.sheet_by_index(0)       # goes to sheet 1
	for i in range (0, size - 1):        # copies training data
		temp0 = sheet.cell(i,0).value
		temp1 = sheet.cell(i,1).value				
		temp2 = sheet.cell(i,2).value
		training_data.append([temp0, temp1, temp2])
	
	w1 = 0				     # initialize weights and internal variables
	w2 = 0
	wb = 0
	iv1 = w1
	iv2 = w2
	iv3 = wb
	vt1 = 10			     # threshold voltages
	vt2 = 20
	vt3 = 30
	vb = 0                               # bias voltage

	tot_error = []
	for j in range (0, epochs):	     # iterate over desired number of epochs
		if j > 0:
			random.shuffle(training_data)				# shuffle training data set
		current_error = 0
		for i in range (0, size - 1):        # perceptron learning algorithm
			input = (w1*training_data[i][0] + w2*training_data[i][1] + wb)
			output = 1/(1 + math.exp(-input))
			error = training_data[i][2] - output
			vb = vt1
			input = error*training_data[i][0] + vb
			iv1 += input - vt1
			vb = vt2
			input = error*training_data[i][1] + vb
			iv2 += input - vt2
			vb = vt3
			input = error + vb
			iv3 += input - vt3
			w1 = iv1
			w2 = iv2
			wb = iv3
			current_error += abs(error) # keeps track of current absolute error in epoch
		tot_error.append(current_error)     # total absolute error for each epoch
		print 'Total error in epoch',j+1,': ',tot_error[j]

	trained_perceptron = [w1, w2, wb, iv1, iv2, tot_error]	
	workbook = xlsxwriter.Workbook('perceptron.xlsx')    # writes the trained network's parameters to an excel file
	worksheet = workbook.add_worksheet()
	worksheet.write('A1', 'w1')
	worksheet.write('A2', trained_perceptron[0])
	worksheet.write('B1', 'w2')
	worksheet.write('B2', trained_perceptron[1])
	worksheet.write('C1', 'wb')
	worksheet.write('C2', trained_perceptron[2])
	workbook.close()
	x = np.arange(1.,epochs+1,1)                         # what to show in x axis. just one by one from 1 to epochs				
	plt.plot(x,trained_perceptron[5],'ro')               # plot total absolute error in y axis 
	plt.xlabel('Epochs')
	plt.ylabel('Sum of absolute error')
	ax = plt.gca()
	ax.get_yaxis().get_major_formatter().set_useOffset(False) # stops scientific notation
	plt.title('Total error for epochs of size 1000')
	plt.show()

