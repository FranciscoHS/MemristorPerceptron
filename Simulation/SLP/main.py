import sys
import training                 # training module
import compute			# computing module
import xlrd	                # allows reading from excel files

# Program structure:
# 1 main file (this one)
# 2 modules:
# training.py, for network training
# compute.py, for using the network to compute


mode = sys.argv[1] # user inputs 't' - training mode
		   # user inputs 'c' - computing mode

if (mode == 't' and len(sys.argv) == 5): # training mode requires as arguments size and name of file where training data is stored 
					 # plus number of epochs
	size = int(sys.argv[2])
	file_name = str(sys.argv[3])
	epochs = int(sys.argv[4])
	training.train(size, file_name, epochs) # runs the training routine

elif (mode == 'c' and len(sys.argv) == 5): # computing mode requires as arguments file where perceptron parameters are stored as well as two inputs
	perceptron_data = []
	file_name = str(sys.argv[2])
	file = xlrd.open_workbook(file_name) # takes perceptron parameters from specified file
	sheet = file.sheet_by_index(0)
	perceptron_data.append(sheet.cell(1,0).value)
	perceptron_data.append(sheet.cell(1,1).value)
	perceptron_data.append(sheet.cell(1,2).value)
	input1 = float(sys.argv[3])          # recovers inputs
	input2 = float(sys.argv[4])

	output = compute.compute(perceptron_data, input1, input2) # runs computing module with aforementioned parameters
	print output

elif (mode == 'test' and len(sys.argv) == 5): 
	perceptron_data = []
	file_name = str(sys.argv[2])
	file = xlrd.open_workbook(file_name) # takes perceptron parameters from specified file
	sheet = file.sheet_by_index(0)
	perceptron_data.append(sheet.cell(1,0).value)
	perceptron_data.append(sheet.cell(1,1).value)
	size = int(sys.argv[3])

	training_data = []                   # takes training data from specified file
	file_name2 = str(sys.argv[4])
	file2 = xlrd.open_workbook(file_name2) 
	sheet2 = file2.sheet_by_index(0)
	predictions = []
	target = []
	success_counter = 0
	for i in range (0, size - 1):        # iterates through training data
					     # calculating output and comparing with target
		training_data.append((sheet2.cell(i,0).value, sheet2.cell(i,1).value))
		predictions.append(compute.compute(perceptron_data,training_data[i][0],training_data[i][1]))
		target.append(sheet2.cell(i,2).value)
		if (predictions[i] - target[i] == 0):
			success_counter += 1
	success_percentage = (float(success_counter)/(size-1))*100
	print 'Success percentage: ', success_percentage
 
else:
	print 'Invalid input'

	
