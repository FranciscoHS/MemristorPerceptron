import sys
import math
import random

class Memristor_Network:
	def activation (self, x):
		return math.tanh(x)

	def activationdiff (self, x):
		return 1-math.tanh(x)**2

	def activationlinear (self, x):
		return x

	def activationlineardiff (self, x):
		return 1

	def setweights(self, weights, bias):
		self.weights = weights
		self.bias = bias
		

	def __init__(self,nInput,nHidden,nOutput):
		self.nInput = nInput					# number of neurons in each layer
		self.nHidden = nHidden
		self.nOutput = nOutput
		self.training_voltage = 10 				# voltage that switches between training and computing mode for memristors

		self.layer_neurons = [nInput, nHidden, nOutput]
		
		self.weights = []
		for i in range (0, len(self.layer_neurons) - 1):	# initialize weights to random value between -1 and 1
			temp_weights = []				# structure: list of layers, each layer has a list of its neurons
			for j in range (0, self.layer_neurons[i]):	# and each neuron has a list of its connections
				temp_temp_weights = []
				for k in range (0, self.layer_neurons[i+1]):
					temp_temp_weights.append((random.random()-0.5)*2)
				temp_weights.append(temp_temp_weights)
			self.weights.append(temp_weights)

		self.bias = []
		for i in range (1, len(self.layer_neurons)): 		# no bias in input layer
			temp = []					# initialize bias weights to random value between -1 and 1
			for j in range (0, self.layer_neurons[i]):	# structure: list of layers, except for input layer
				temp.append((random.random()-0.5)*2) 	# each layer has a list with bias weights values for 
			self.bias.append(temp)				# each of its neurons

		self.values = []					# initialize pre activation values to 0
		for i in range (0, len(self.layer_neurons)):		# structure: list of layers
			temp = []					# each layer has a list with its neurons' pre activation values
			for j in range (0, self.layer_neurons[i]):
				temp.append(0)
			self.values.append(temp)

		self.active_values = []					# initialize post activation values to 0
		for i in range (0, len(self.layer_neurons)):		# same structure
			temp = []
			for j in range (0, self.layer_neurons[i]):
				temp.append(0)
			self.active_values.append(temp)

		self.delta = []						# initialize local gradients to 0
		for i in range (0, len(self.layer_neurons)):		# same structure
			temp = []
			for j in range (0, self.layer_neurons[i]):
				temp.append(0)
			self.delta.append(temp)

		self.link_memristors_iv = self.weights			# initialize internal variables, with same structures as weights and bias
		self.node_memristors_iv = self.bias
		self.voltages = self.link_memristors_iv + self.node_memristors_iv 

		self.voltages = []					# initialize training/computing voltages to 0
		for i in range (0, len(self.link_memristors_iv)):	# structure is weights followed by bias
			temp = []
			for j in range (0, len(self.link_memristors_iv[i])):
				temp_temp = []
				for k in range (0, len(self.link_memristors_iv[i][j])):
					temp_temp.append(0)
				temp.append(temp_temp)
			self.voltages.append(temp)
		for i in range (0, len(self.node_memristors_iv)):
			temp = []
			for j in range (0, len(self.node_memristors_iv[i])):
				temp.append(0)
			self.voltages.append(temp)

	def compute(self, inputs):
		if len(inputs) != self.nInput:
			print 'Invalid input format'
			sys.exit()

		for j in range (0, len(self.values)):				# reset values
			for l in range (0, len(self.values[j])):
				self.values[j][l] = 0
				self.active_values[j][l] = 0

		self.inputs = inputs
		for i in range (0, self.layer_neurons[0]):
			self.values[0][i] = inputs[i] 			# set input layer values
			self.active_values[0][i] = self.values[0][i] 
		
		for i in range (1, len(self.layer_neurons) - 1):        # calculate hidden layers values
			for j in range (0, self.layer_neurons[i]):
				for k in range (0, self.layer_neurons[i-1]):
					self.values[i][j] += self.active_values[i-1][k]*self.weights[i-1][k][j]
				self.values[i][j] += self.bias[i-1][j]
				self.active_values[i][j] = self.activation(self.values[i][j])

		for i in range (0, self.layer_neurons[len(self.layer_neurons) - 1]): 		# output layer
			for j in range (0, self.layer_neurons[len(self.layer_neurons) - 2]):
				self.values[len(self.layer_neurons) - 1][i] += self.active_values[len(self.layer_neurons) - 2][j]*self.weights[len(self.layer_neurons) - 2][j][i]	
				self.values[len(self.layer_neurons) - 1][i] += self.bias[len(self.layer_neurons) - 2][i]
				self.active_values[len(self.layer_neurons) - 1][i] = self.activationlinear(self.values[len(self.layer_neurons) - 1][i])

		return self.active_values[len(self.layer_neurons) - 1][0]


	def training(self, training_data, learning_rate, batch_size):
		total_error = 0
		batch_count = 0
		training_error = 0
		for i in range(0, len(training_data)):
		# RESET EVERYTHING

			for j in range (0, len(self.values)):
				for l in range (0, len(self.values[j])):
					self.values[j][l] = 0
					self.active_values[j][l] = 0
					self.delta[j][l] = 0

		# FORWARD PROPAGATION

			for j in range (0, self.layer_neurons[0]):
				self.values[0][j] = training_data[i][j] 			# set input layer values
				self.active_values[0][j] = self.values[0][j]
		
			for m in range (1, len(self.layer_neurons) - 1):        		# calculate hidden layers values
				for j in range (0, self.layer_neurons[m]):
					for k in range (0, self.layer_neurons[m-1]):
						self.values[m][j] += self.active_values[m-1][k]*self.weights[m-1][k][j]
					self.values[m][j] += self.bias[m-1][j]
					self.active_values[m][j] = self.activation(self.values[m][j])

			for m in range (0, self.layer_neurons[len(self.layer_neurons) - 1]):
				for j in range (0, self.layer_neurons[len(self.layer_neurons) - 2]):
					self.values[len(self.layer_neurons) - 1][m] += self.active_values[len(self.layer_neurons) - 2][j]*self.weights[len(self.layer_neurons) - 2][j][m]	
					self.values[len(self.layer_neurons) - 1][m] += self.bias[len(self.layer_neurons) - 2][m]
					self.active_values[len(self.layer_neurons) - 1][m] = self.activationlinear(self.values[len(self.layer_neurons) - 1][m])


			desired_output = training_data[i][len(training_data[i]) - 1]
			error = desired_output - self.active_values[len(self.layer_neurons) - 1][0]
			training_error += error
			current_error = 0.5*error**2
			total_error += current_error

			for i in range (0, len(self.voltages)):
				for j in range (0, len(self.voltages[i])):
					if type(self.voltages[i][j]) is list:
						for k in range (0, len(self.voltages[i][j])):
							self.voltages[i][j][k] = self.training_voltage
					else:
						self.voltages[i][j] = self.training_voltage
			

		# BACKPROPAGATION
			if (batch_count == batch_size or len(training_data) - i <= batch_size):	
				if (batch_count == batch_size):
					training_error = training_error/batch_size
				else:
					training_error = training_error/(len(training_data) - i)
				batch_count = 0
				k = len(self.layer_neurons) - 1
				while (k > 0):
					if k == len(self.layer_neurons) - 1:		# correction to weights between output layer and first hidden layer
						self.delta[k][0] = self.activationlineardiff(self.values[k][0])*training_error
						for l in range (0, self.layer_neurons[k-1]):
							correction_input = learning_rate*self.active_values[k-1][l]*self.delta[k][0] + self.voltages[len(self.weights) - 1][l][0]
							self.link_memristors_iv[len(self.link_memristors_iv) - 1][l][0] += correction_input - self.training_voltage

							self.weights[len(self.weights) - 1][l][0] = self.link_memristors_iv[len(self.link_memristors_iv) - 1][l][0]
						correction_input = learning_rate*self.delta[k][0] + self.voltages[len(self.voltages) - 1][0]
						self.node_memristors_iv[k-1][0] += correction_input - self.training_voltage
						self.bias[k-1][0] = self.node_memristors_iv[k-1][0]
					
					else:
						for l in range (0, self.layer_neurons[k]):
							if k == len(self.layer_neurons) - 2: # first hidden layer from the right
								self.delta[k][l] = self.activationdiff(self.values[k][l])*self.delta[k+1][0]*self.weights[k][l][0]
							else:
								for m in range (0, self.layer_neurons[k+1]):
									self.delta[k][l] += self.activationdiff(self.values[k][l])*self.delta[k+1][m]*self.weights[k][l][m]
						
							for m in range (0, self.layer_neurons[k-1]):
								correction_input = learning_rate*self.delta[k][l]*self.active_values[k-1][m] + self.voltages[k-1][m][l]
								self.link_memristors_iv[k-1][m][l] += correction_input - self.training_voltage
								self.weights[k-1][m][l] = self.link_memristors_iv[k-1][m][l]
							correction_input = learning_rate*self.delta[k][l] + self.voltages[len(self.weights) + k - 1][l]
							self.node_memristors_iv[k-1][0] += correction_input - self.training_voltage
							self.bias[k-1][l] = self.node_memristors_iv[k-1][0]
					k -= 1
			batch_count += 1

		return (self.weights, self.bias, total_error)
			
		

