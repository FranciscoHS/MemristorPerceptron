import sys
import math
import random
import matplotlib.pyplot as plt # makes pretty plots
import numpy as np
import MLP
import csv

if (len(sys.argv) == 2 and str(sys.argv[1]) == 't'):
	layer_number = int(input('Number of layers?'))
	layer_neurons = []
	if (type(layer_number) is int and layer_number > 0):
		for i in range (1,layer_number+1):
			input_message = 'Neurons in layer ' + str(i) + '?'
			layer_neurons.append(int(input(input_message)))
	else:
		print 'Invalid input'

	network = MLP.Memristor_Network(layer_neurons[0], layer_neurons[1], layer_neurons[2])

	learning_rate = 0.00001
	epochs = int(input('How many epochs?'))
	size = int(input('Size of data set?'))
	batch_size = int(input('Size of batches?'))
	training_data = []
	for i in range(0, size):
		x = random.random()*5
		training_data.append((x, math.sin(x)))

	tot_error = []
	for i in range (0, epochs):
		if i > 0:
			random.shuffle(training_data)
		tot_error.append(network.training(training_data, learning_rate, batch_size)[2])

	x = np.arange(1.,epochs+1,1)                         # what to show in x axis. just one by one from 1 to epochs				
	plt.plot(x,tot_error,'ro')               	     # plot total absolute error in y axis 
	plt.xlabel('Epochs')
	plt.ylabel('Sum of absolute error')
	ax = plt.gca()
	ax.get_yaxis().get_major_formatter().set_useOffset(False) # stops scientific notation
	title = 'Total error for epochs of size '
	title += str(size)
	plt.title(title)
	plt.show()

	file_name = 'weights.csv'
	csv_file = open(file_name, 'wb')
	writer = csv.writer(csv_file, delimiter=',')

	data_out = []
	for layer in network.weights:
		for neuron in layer:
			temp = []
			for connection in neuron:
				temp.append(connection)
			data_out.append(temp)

	for layer in data_out:
		writer.writerow(layer)					# weights are written at 1 neuron per row

	temp = []
	for layer in network.layer_neurons:				# writes structure of network
		temp.append(layer)
	writer.writerow(temp)

	csv_file.close()

	file_name = 'bias.csv'
	csv_file = open(file_name, 'wb')
	writer = csv.writer(csv_file, delimiter=',')

	data_out = []
	for layer in network.bias:
		temp = []
		for neuron in layer:
			temp.append(neuron)
		data_out.append(temp)

	for layer in data_out:
		writer.writerow(layer)

	csv_file.close()

elif (len(sys.argv) == 4 and str(sys.argv[1]) == 'c'):
	file_name = str(sys.argv[2])
	try:
		csv_file_1 = open(file_name, 'rb')
	except:
		print 'File not found.'
		sys.exit()
	datareader_1 = csv.reader(csv_file_1, delimiter=',', quotechar='|') 
	weights_read = []
	struct = []

	row_count = sum(1 for row in datareader_1)
	csv_file_1.seek(0)

	count = 0
	for row in datareader_1:
		if count == row_count - 1:
			for cell in row:
				struct.append(int(cell))
		count += 1

	csv_file_1.seek(0)

	count1 = 1
	count2 = 0
	count3 = 0
	temp1 = []
	temp2 = []
	for row in datareader_1:
		if count3 == row_count - 1:
			break
		count3 += 1

		for cell in row:
			temp1.append(float(cell))
		temp2.append(temp1)
		temp1 = []
		if count1 == struct[count2]:
			count1 = 0
			count2 += 1
			weights_read.append(temp2)
			temp2 = []
		count1 += 1
		
	file_name = str(sys.argv[3])

	try:
		csv_file = open(file_name, 'rb')
	except:
		print 'File not found.'
		sys.exit()

	datareader_1 = csv.reader(csv_file, delimiter=',', quotechar='|') 
	bias_read = []

	for row in datareader_1:
		temp = []
		for i in range (0, len(row)):
			temp.append(float(row[i]))
		bias_read.append(temp)

	actual = []
	estimate = []
	x_values = []
	iterations = int(input('Run how many times?'))
	network = MLP.Memristor_Network(struct[0], struct[1], struct[2])
	network.setweights(weights_read, bias_read)

	for i in range(0, iterations):
		x = [random.random()*5]
		output = network.compute(x) # runs computing module 
		estimate.append(output)
		x_values.append(x[0])
		actual.append(math.sin(x[0]))

	x1 = np.array(x_values)
	y1 = np.array(estimate)
	y2 = np.array(actual) 
	plt.plot(x1, y2, 'bo') 
	plt.plot(x1, y1,'ro')               	     
	plt.xlabel('x')
	plt.ylabel('sin(x)')
	title = 'Red - Network; Blue - "Real"'
	plt.title(title)
	plt.show()

else:
	print 'Invalid input'
	sys.exit()



