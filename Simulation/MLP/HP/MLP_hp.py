import random
import sys
import math

class Network:
	def __init__(self, inputs, network_structure, learning_rate):
		self.training_charge = 0 				# charge that switches between training and computing mode for memristors
		self.learning_rate = learning_rate
		self.wmax = 100 					# max weight value. used to enable rescaling for iv
		connections = []
		nodes = []
		connections.append(inputs*int(network_structure[0]))
		for i in range (0, len(network_structure)):
			nodes.append(int(network_structure[i]))
			if i < len(network_structure) - 1:
				connections.append(int(network_structure[i])*int(network_structure[i+1]))

		self.connection_memristors = []
		self.weights = []

		connection_count = 0
		aux = 0
		for layer in connections:
			temp = []
			for i in range(0, int(layer)):
				connection_count += 1
				if aux == 0:
					temp.append(Connection_Memristor(inputs, nodes[aux]))
				else:
					temp.append(Connection_Memristor(nodes[aux-1],nodes[aux]))
			aux += 1		
			self.connection_memristors.append(temp)

		for layer in self.connection_memristors:
			temp = []
			for memristor in layer:
				temp.append(memristor.iv)
			self.weights.append(temp)

		self.node_memristors = []
		self.bias = []

		node_count = 0
		for layer in nodes:
			temp = []
			for i in range(0, int(layer)):
				node_count += 1
				temp.append(Node_Memristor())
			self.node_memristors.append(temp)

		self.values = []
		self.active_values = []
		self.delta = []
						
		for layer in self.node_memristors:
			temp = []
			temp_val = []
			temp_active_val = []
			temp_delta = []
			for memristor in layer:
				temp.append(memristor.iv)
				temp_val.append(0)
				temp_active_val.append(0)
				temp_delta.append(0)
			self.values.append(temp_val)
			self.active_values.append(temp_active_val)
			self.delta.append(temp_delta)
			self.bias.append(temp)	

		print ('There are', connection_count, 'connection memristor(s) and', node_count, 'node memristor(s) in this network.')	

	def compute(self, inputs):
		for i in range (0, len(self.values)):				# reset all values before each computation
			for j in range (0, len(self.values[i])):
				self.values[i][j] = 0
				self.active_values[i][j] = 0
		temp = 0
		for j in range (0, len(self.node_memristors[0])):		# first layer, using inputs
			for k in range (0, len(inputs)):
				self.values[0][j] += inputs[k]*self.weights[0][k+temp]
			temp += len(inputs)
				
			self.values[0][j] += self.bias[0][j]
			if len(self.node_memristors[0]) > 1:
				self.active_values[0][j] = self.node_memristors[0][j].activation(self.values[0][j])
			else:
				self.active_values[0][j] = self.node_memristors[0][j].activation_final(self.values[0][j])

		for i in range (1, len(self.node_memristors)):
			temp = 0
			for j in range (0, len(self.node_memristors[i])):
				for k in range (0, len(self.node_memristors[i-1])):
					self.values[i][j] += self.active_values[i-1][k]*self.weights[i][k+temp]
				temp += len(self.node_memristors[i-1])
	
				self.values[i][j] += self.bias[i][j]
				if i < len(self.node_memristors) - 1:
					self.active_values[i][j] = self.node_memristors[i][j].activation(self.values[i][j])
				else:
					self.active_values[i][j] = self.node_memristors[i][j].activation_final(self.values[i][j])
		return self.active_values[len(self.node_memristors) - 1][0]


	def training(self, training_data):
		total_error = 0
		for t in range (0, len(training_data)):	
			for i in range (0, len(self.values)):				# reset all values before each computation
				for j in range (0, len(self.values[i])):
					self.values[i][j] = 0
					self.active_values[i][j] = 0
					self.delta[i][j] = 0

		# FORWARD PROPAGATION
			temp = 0
			for j in range (0, len(self.node_memristors[0])):		# first layer, using training_data
				for k in range (0, len(training_data[t]) - 1):
					self.values[0][j] += training_data[t][k]*self.weights[0][k+temp]
				#temp += len(training_data[t]) - 1
				temp += len (self.node_memristors[0])
					
				self.values[0][j] += self.bias[0][j]
				if len(self.node_memristors) > 1:
					self.active_values[0][j] = self.node_memristors[0][j].activation(self.values[0][j])
				else:
					self.active_values[0][j] = self.node_memristors[0][j].activation_final(self.values[0][j])

			
			for i in range (1, len(self.node_memristors)):
				temp = 0
				for j in range (0, len(self.node_memristors[i])):
					for k in range (0, len(self.node_memristors[i-1])):
						self.values[i][j] += self.active_values[i-1][k]*self.weights[i][k+temp]
					#temp += len(self.node_memristors[i-1])
					temp += len(self.node_memristors[i])
					
					self.values[i][j] += self.bias[i][j]
					if i == len(self.node_memristors) - 1:
						self.active_values[i][j] = self.node_memristors[i][j].activation_final(self.values[i][j])
					else:
						self.active_values[i][j] = self.node_memristors[i][j].activation(self.values[i][j])
			# BACKPROPAGATION
			desired_output = training_data[t][len(training_data[t]) - 1]
			error = desired_output - self.active_values[len(self.node_memristors) - 1][0]
			current_error = 0.5*error**2
			total_error += current_error

			k = len(self.node_memristors) - 1

			while (k >= 0):
				if k == len(self.node_memristors) - 1 and len(self.node_memristors) == 1:	# in case there is only 
																							# one layer of nodes
					self.delta[k][0] = self.node_memristors[k][0].activation_finaldiff(self.values[k][0])*error
					for l in range (0, len(self.connection_memristors[k])):
						correction_input = self.learning_rate*training_data[t][l]*self.delta[k][0] + self.training_charge
						self.connection_memristors[k][l].iv_change(correction_input - self.training_charge)
						self.weights[k][l] = self.connection_memristors[k][l].iv/self.connection_memristors[k][l].thickness

					correction_input = self.learning_rate*self.delta[k][0] + self.training_charge
					self.node_memristors[k][0].iv_change(correction_input - self.training_charge)
					self.bias[k][0] = self.node_memristors[k][0].iv/self.node_memristors[k][0].thickness

				elif k == len(self.node_memristors) - 1:	# to the left of output when there's at least one hidden layer
					self.delta[k][0] = self.node_memristors[k][0].activation_finaldiff(self.values[k][0])*error
					for l in range (0, len(self.connection_memristors[k])):
						correction_input = self.learning_rate*self.active_values[k-1][l]*self.delta[k][0]+self.training_charge
						self.connection_memristors[k][l].iv_change(correction_input - self.training_charge)
						self.weights[k][l] = self.connection_memristors[k][l].iv/self.connection_memristors[k][l].thickness

					correction_input = self.learning_rate*self.delta[k][0] + self.training_charge
					self.node_memristors[k][0].iv_change(correction_input - self.training_charge)
					self.bias[k][0] = self.node_memristors[k][0].iv/self.node_memristors[k][0].thickness							
				else:
					temp = 0
					temp2 = 0
					temp3 = 0
					for l in range (0, len(self.node_memristors[k])):							  # compute local gradients (deltas)
						for m in range (0, len(self.node_memristors[k+1])):
							#self.delta[k][l] += self.node_memristors[k][l].activationdiff(self.values[k][l])*self.delta[k+1][m]*self.weights[k][m+temp]
							self.delta[k][l] += self.node_memristors[k][l].activationdiff(self.values[k][l])*self.delta[k+1][m]*self.weights[k+1][m+temp]
							
						temp += len(self.node_memristors[k+1])
							
						if k - 1 >= 0:
							for m in range (0, len(self.node_memristors[k-1])):
								correction_input = self.learning_rate*self.active_values[k-1][m]*self.delta[k][l] + self.training_charge
								#self.connection_memristors[k][m + temp2].iv_change(correction_input - self.training_charge)
								#self.weights[k][m + temp2] = self.connection_memristors[k][m+temp2].iv/self.connection_memristors[k][m+temp2].thickness
								self.connection_memristors[k-1][m + temp2].iv_change(correction_input - self.training_charge)
								self.weights[k-1][m + temp2] = self.connection_memristors[k-1][m+temp2].iv/self.connection_memristors[k][m+temp2].thickness		

							temp2 += len(self.node_memristors[k])
							
							correction_input = self.learning_rate*self.delta[k][l] + self.training_charge
							self.node_memristors[k][l].iv_change(correction_input - self.training_charge)
							self.bias[k][l] = self.node_memristors[k][l].iv/self.node_memristors[k][l].thickness		
						else:									# in case this is the first hidden layer from the left
							for m in range (0, len(training_data[t]) - 1):
								correction_input = self.learning_rate*training_data[t][m]*self.delta[k][l] + self.training_charge
								#self.connection_memristors[k][m + temp3].iv_change(correction_input - self.training_charge)
								#self.weights[k][m + temp3] = self.connection_memristors[k][m+temp2].iv/self.connection_memristors[k][m+temp3].thickness
								self.connection_memristors[k][m + temp3].iv_change(correction_input - self.training_charge)
								self.weights[k][m + temp3] = self.connection_memristors[k][m+temp3].iv/self.connection_memristors[k][m+temp3].thickness								
							temp3 += len(self.node_memristors[k])
							
							correction_input = self.learning_rate*self.delta[k][l] + self.training_charge
							self.node_memristors[k][l].iv_change(correction_input - self.training_charge)
							self.bias[k][l] = self.node_memristors[k][l].iv/self.node_memristors[k][l].thickness		
							
							
				k -= 1
		return total_error

class Node_Memristor:
	def __init__(self):
		self.iv = 0
		self.mu = 1
		self.thickness = 1
		self.r_off = 1
		self.r_on = 1

	"""
		self.iv = (random.random()-0.5)*2
		self.mu = 10**(-14)					# physical memristor parameters based on HP paper
		self.thickness = 10**(-8)
		self.r_off = 300
		self.r_on = 1
	"""
		
	def activation (self, i):
		return math.tanh(i)

	def activationdiff (self, i):
		return 1/(math.cosh(i)**2)
		
	def activation_final (self, i):
		return (math.tanh(i) + 1)/2
	
	def activation_finaldiff (self, i):
		return 0.5/(math.cosh(i)**2)

	def activationlinear (self, i):
		return i

	def activationlineardiff (self, i):
		return 1

	def iv_change (self, i):
		i = i*self.thickness/self.mu/self.r_on
		self.iv += self.mu*self.r_on*i/self.thickness
"""		if self.iv < 0:
			self.iv = 0
		if self.iv > self.thickness:
			self.iv = self.thickness
"""
class Connection_Memristor:
	def __init__(self, n_in, n_out):
		self.iv = (random.random()-0.5)*2*math.sqrt(6/(n_in+n_out))			# Xavier/Glorot uniform initialization scheme
		self.mu = 1
		self.thickness = 1
		self.r_off = 1
		self.r_on = 1

	"""	self.iv = (random.random()-0.5)*2
		self.mu = 10**(-14)					# physical memristor parameters based on HP paper
		self.thickness = 10**(-8)
		self.r_off = 300
		self.r_on = 1
"""
	def iv_change (self, i):
		i = i*self.thickness/self.mu/self.r_on
		self.iv += self.mu*self.r_on*i/self.thickness
"""		if self.iv < 0:						# memristor equations only valid for iv between 0 and thickness
			self.iv = 0
		if self.iv > self.thickness:
			self.iv = self.thickness
"""		
			
		
