import MLP_hp
import random
import matplotlib.pyplot as plt 
import numpy as np
import math
import sys
import copy

inputs = int(input('How many inputs?'))
layer_number = int(input('How many layers in network?'))
network_structure = []
for i in range (0, layer_number):
	question = 'How many neurons in layer ' + str(i + 1) + '?'
	network_structure.append(int(input(question)))

learning_rate = 0.1
network = MLP_hp.Network(inputs, network_structure, learning_rate)

training_data = []
"""
for i in range (0, 100):
	x = round(random.random())
	y = round(random.random())
	if x == 0 and y == 0:
		z = 0
	else:
		z = 1
	training_data.append((x, y, z))
"""
"""
for i in range (0, 1000):
	x = round(random.random())
	y = round(random.random())
	training_data.append((x,y,x*y))
"""

for i in range (0, 100):
	x = round (random.random())
	y = round (random.random())
	if x != y:
		training_data.append((x, y, 1))
	else:
		training_data.append((x, y, 0))


training_data.append((0,0,0))
training_data.append((0,1,1))
training_data.append((1,0,1))
training_data.append((1,1,0))

epochs = 100
	
tot_error = []	
weight_history = []
weight_history.append(copy.deepcopy(network.weights))	
bias_history = []
bias_history.append(copy.deepcopy(network.bias))


for j in range (0, epochs):
	if j > 0:
		random.shuffle(training_data)
	tot_error.append(network.training(training_data))
	weight_history.append(copy.deepcopy(network.weights))
	bias_history.append(copy.deepcopy(network.bias))

plot_bias = []

for i in range (0, layer_number):
	for j in range (0, network_structure[i]):
		temp = []
		for k in range (0, len(bias_history)):
			temp.append(bias_history[k][i][j])
		plot_bias.append(temp)		
	
count = 1	
for bias in plot_bias:
	x = np.arange(1.,epochs + 2,1)
	plt.plot(x,bias)
	ax = plt.gca()
	ax.get_yaxis().get_major_formatter().set_useOffset(False)
	title = "Bias ", count 	
	plt.title(title)
	plt.show()
	count += 1
			
plot_weights = []	
	
for i in range (0, layer_number):
	for j in range (0, len(network.weights[i])):
		temp = []
		for k in range (0, len(weight_history)):
			temp.append(weight_history[k][i][j])
		plot_weights.append(temp)	

#print (plot_weights)		
	
count = 1		
for weight in plot_weights:
	x = np.arange(1.,epochs + 2,1)
	plt.plot(x,weight)
	ax = plt.gca()
	ax.get_yaxis().get_major_formatter().set_useOffset(False) 
	title = "Weight ", count 	
	plt.title(title)
	plt.show()
	count += 1

		
x = np.arange(1.,epochs + 1,1)                      	  # what to show in x axis. just one by one from 1 to epochs				
plt.plot(x,tot_error)               	         	      # plot total absolute error in y axis 
plt.xlabel('Epochs')
plt.ylabel('Sum of error')
ax = plt.gca()
ax.get_yaxis().get_major_formatter().set_useOffset(False) # stops scientific notation
title = 'Total error for epochs of size 4'
plt.title(title)
plt.show()

print ('Input: (0,0)')
output = network.compute((0,0))
print (output)
print ('Input: (0,1)')
output = network.compute((0,1))
print (output)
print ('Input: (1,0)')
output = network.compute((1,0))
print (output)
print ('Input: (1,1)')
output = network.compute((1,1))
print (output)

"""
i = 10
while (i > 0):
	print (tot_error[epochs-i])
	i -= 1
"""	
